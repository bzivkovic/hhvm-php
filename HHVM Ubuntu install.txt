﻿https://github.com/facebook/hhvm/wiki/Prebuilt-packages-on-Ubuntu-14.04


# installs add-apt-repository
sudo apt-get install software-properties-common

sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0x5a16e7281be7a449
sudo add-apt-repository 'deb http://dl.hhvm.com/ubuntu trusty main'
sudo apt-get update
sudo apt-get install hhvm




# Install nginx
========================
sudo apt-get install nginx


# HHVM fastcgi config
========================
sudo /usr/share/hhvm/install_fastcgi.sh


# Restart Nginx & HHVM
========================
sudo /etc/init.d/nginx restart
sudo /etc/init.d/hhvm restart


# Create index.php file
========================
sudo cd /usr/share/nginx/html
sudo vim index.php